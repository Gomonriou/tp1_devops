FROM node:alpine

ENV PORT=80
ENV NODE_ENV development
ENV MONGO_HOST localhost
ENV MONGO_PORT 27017
ENV MONGO_USER root
ENV MONGO_PASSWORD 123456
ENV MONGO_DATABASE test

WORKDIR /app
COPY package*.json ./
RUN npm install && npm init
COPY . .
EXPOSE 80
CMD ["node","app.js"]